
#include "rvc.h"
#include "CAN.h"



struct RVC::node {
    uint8_t     address,


    void *      reset_callback = NULL,
    void *      download_callback = NULL,
    void *      terminal_callback = NULL,

} RVC::node_config;

RVC::RVC(const uint8_t cs_rx=0, const uint8_t int_tx=0) {
    // Set the cs/rx and int/tx pins if defined
    if (cs_rx != 0) {
        CAN.setPins(cs_rx, int_tx);
    }

    CAN.begin(250E3);

}


void RVC::setAddress(uint8_t addr) {
    RVC::node_config.address = addr;

    // perform an ADDRESS_CLAIM

}

void RVC::sendPGN(const long_t pgn, const int8_t priority, const uint8_t *data[], const int8_t length) {

    CAN.beginExtendedPacket(RVC::arbitration_field(RVC::node_config.address,
                                               pgn, priority));
    CAN.write(length);
    CAN.write(data, length);
    CAN.endPacket();
}

void RVC::sendBlock(){

}

long_t RVC::arbitration_field(const uint8_t addr, const long_t pgn, const int8_t priority) {
    uint8_t dgn_hi, dgn_lo;
    long_t header;

    dgn_lo = pgn & 0xFF;
    dgn_hi = pgn >> 8;

    header = (priority & 0x07) << 26 + dgn_hi << 16 + dgn_lo << 8 + (addr & 0xFF) << 1;
    return header;
}

// required PGNs to be supported


void RVC::request_dgn(const long_t dgn, const uint8_t dest = 255) {

    CAN.beginExtendedPacket(RVC::arbitration_field(RVC::node_config.address,
                                               PGN_DGN_REQUEST + dest, 6));
    CAN.write(5);
    CAN.write(dgn & 0xFF);
    CAN.write((dgn >> 8) & 0xFF);
    CAN.write((dgn >> 16) & 0xFF);
    CAN.write(255);
    CAN.write(255);
    CAN.endPacket();
}


void RVC::acknowledge(const uint8_t dest = 255, )
// optional PGNs to be supported

RVC::setResetCallback(void *callback) {
    RVC::node_config.reset_callback = callback;
}



