
#ifndef RVC_H
#define RVC_H

#include "Arduino.h"

#define PGN_DM_RV                                       0x1FECA
#define PGN_ADDRESS_CLAIM                               0x0
#define PGN_DGN_REQUEST                                 0xEA00


#define PGN_DATE_TIME_STATUS				            0x1FFFF
#define PGN_SET_DATE_TIME_COMMAND	        			0x1FFFE
#define PGN_DC_SOURCE_STATUS_1				            0x1FFFD
#define PGN_DC_SOURCE_STATUS_2	            			0x1FFFC
#define PGN_DC_SOURCE_STATUS_3			            	0x1FFFB
#define PGN_COMMUNICATION_STATUS_1      				0x1FFFA
#define PGN_COMMUNICATION_STATUS_2			        	0x1FFF9
#define PGN_COMMUNICATION_STATUS_3	        			0x1FFF8
#define PGN_WATERHEATER_STATUS				            0x1FFF7
#define PGN_WATERHEATER_COMMAND		            		0x1FFF6
#define PGN_GAS_SENSOR_STATUS	            			0x1FFF5
#define PGN_CHASSIS_MOBILITY_STATUS				        0x1FFF4
#define PGN_CHASSIS_MOBILITY_COMMAND		    		0x1FFF3
#define PGN_AAS_CONFIG_STATUS	            			0x1FFF2
#define PGN_AAS_CONFIG_COMMAND				            0x1FFF1
#define PGN_AAS_STATUS	                    			0x1FFF0 // deprecated?
#define PGN_AAS_SENSOR_STATUS	            			0x1FFEF
#define PGN_LEVELING_CONTROL_COMMAND		    		0x1FFEE
#define PGN_LEVELING_CONTROL_STATUS	        			0x1FFED
#define PGN_LEVELING_JACK_STATUS				        0x1FFEC
#define PGN_LEVELING_SENSOR_STATUS		        		0x1FFEB
#define PGN_HYDRAULIC_PUMP_STATUS       				0x1FFEA
#define PGN_LEVELING_AIR_STATUS		            		0x1FFE9
#define PGN_SLIDE_STATUS		                		0x1FFE8
#define PGN_SLIDE_COMMAND	                			0x1FFE7
#define PGN_SLIDE_SENSOR_STATUS	            			0x1FFE6
#define PGN_SLIDE_MOTOR_STATUS				            0x1FFE5
#define PGN_FURNACE_STATUS	                			0x1FFE4
#define PGN_FURNACE_COMMAND	                			0x1FFE3
#define PGN_THERMOSTAT_STATUS_1		            		0x1FFE2
#define PGN_AIR_CONDITIONER_STATUS		        		0x1FFE1
#define PGN_AIR_CONDITIONER_COMMAND			        	0x1FFE0
#define PGN_GENERATOR_AC_STATUS_1		        		0x1FFDF
#define PGN_GENERATOR_AC_STATUS_2       				0x1FFDE
#define PGN_GENERATOR_AC_STATUS_3				        0x1FFDD
#define PGN_GENERATOR_STATUS_1		            		0x1FFDC
#define PGN_GENERATOR_STATUS_2		            		0x1FFDB
#define PGN_GENERATOR_COMMAND				            0x1FFDA
#define PGN_GENERATOR_START_CONFIG_STATUS				0x1FFD9
#define PGN_GENERATOR_START_CONFIG_COMMAND				0x1FFD8
#define PGN_INVERTER_AC_STATUS_1	        			0x1FFD7
#define PGN_INVERTER_AC_STATUS_2			        	0x1FFD6
#define PGN_INVERTER_AC_STATUS_3	        			0x1FFD5
#define PGN_INVERTER_STATUS				                0x1FFD4
#define PGN_INVERTER_COMMAND            				0x1FFD3
#define PGN_INVERTER_CONFIGURATION_STATUS_1				0x1FFD2
#define PGN_INVERTER_CONFIGURATION_STATUS_2				0x1FFD1
#define PGN_INVERTER_CONFIGURATION_COMMAND_1			0x1FFD0
#define PGN_INVERTER_CONFIGURATION_COMMAND_2			0x1FFCF
#define PGN_INVERTER_STATISTICS_STATUS				    0x1FFCE
#define PGN_INVERTER_APS_STATUS			            	0x1FFCD
#define PGN_INVERTER_DCBUS_STATUS		        		0x1FFCC
#define PGN_INVERTER_OPS_STATUS			            	0x1FFCB
#define PGN_CHARGER_AC_STATUS_1			            	0x1FFCA
#define PGN_CHARGER_AC_STATUS_2				            0x1FFC9
#define PGN_CHARGER_AC_STATUS_3			            	0x1FFC8
#define PGN_CHARGER_STATUS			                	0x1FFC7
#define PGN_CHARGER_CONFIGURATION_STATUS				0x1FFC6
#define PGN_CHARGER_COMMAND	                			0x1FFC5
#define PGN_CHARGER_CONFIGURATION_COMMAND				0x1FFC4
#define PGN_CHARGER_STATISTICS_STATUS			       	0x1FFC3 // deprecated?
#define PGN_CHARGER_APS_STATUS		            		0x1FFC2
#define PGN_CHARGER_DCBUS_STATUS			        	0x1FFC1
#define PGN_CHARGER_OPS_STATUS		            		0x1FFC0
#define PGN_AC_LOAD_STATUS		                		0x1FFBF
#define PGN_AC_LOAD_COMMAND			                	0x1FFBE
#define PGN_DC_LOAD_STATUS	                			0x1FFBD
#define PGN_DC_LOAD_COMMAND	                			0x1FFBC
#define PGN_DC_DIMMER_STATUS_1		            		0x1FFBB
#define PGN_DC_DIMMER_STATUS_2				            0x1FFBA
#define PGN_DC_DIMMER_COMMAND	            			0x1FFB9
#define PGN_DIGITAL_INPUT_STATUS				        0x1FFB8
#define PGN_TANK_STATUS		                    		0x1FFB7
#define PGN_TANK_CALIBRATION_COMMAND			    	0x1FFB6
#define PGN_TANK_GEOMETRY_STATUS		        		0x1FFB5
#define PGN_TANK_GEOMETRY_COMMAND			        	0x1FFB4
#define PGN_WATER_PUMP_STATUS	            			0x1FFB3
#define PGN_WATER_PUMP_COMMAND				            0x1FFB2
#define PGN_AUTOFILL_STATUS	                			0x1FFB1
#define PGN_AUTOFILL_COMMAND			            	0x1FFB0
#define PGN_WASTEDUMP_STATUS            				0x1FFAF
#define PGN_WASTEDUMP_COMMAND				            0x1FFAE
#define PGN_ATS_AC_STATUS_1	                			0x1FFAD
#define PGN_ATS_AC_STATUS_2	                			0x1FFAC
#define PGN_ATS_AC_STATUS_3	                			0x1FFAB
#define PGN_ATS_STATUS	                    			0x1FFAA
#define PGN_ATS_COMMAND	                    			0x1FFA9
#define PGN_WEATHER_STATUS_1			            	0x1FFA5
#define PGN_WEATHER_STATUS_2            				0x1FFA4
#define PGN_ALTIMETER_STATUS			            	0x1FFA3
#define PGN_ALTIMETER_COMMAND           				0x1FFA2
#define PGN_WEATHER_CALIBRATE_COMMAND		       		0x1FFA1
#define PGN_COMPASS_BEARING_STATUS	        			0x1FFA0
#define PGN_COMPASS_CALIBRATE_COMMAND				    0x1FF9F
#define PGN_BRIDGE_COMMAND			                	0x1FF9E // deprecated?
#define PGN_BRIDGE_PGN_LIST	                   			0x1FF9D // deprecated?
#define PGN_THERMOSTAT_AMBIENT_STATUS			    	0x1FF9C
#define PGN_HEAT_PUMP_STATUS		            		0x1FF9B
#define PGN_HEAT_PUMP_COMMAND				            0x1FF9A
#define PGN_CHARGER_EQUALIZATION_STATUS	    			0x1FF99
#define PGN_CHARGER_EQUALIZATION_CONFIGURATION_STATUS	0x1FF98
#define PGN_CHARGER_EQUALIZATION_CONFIGURATION_COMMAND	0x1FF97
#define PGN_CHARGER_CONFIGURATION_STATUS_2				0x1FF96
#define PGN_CHARGER_CONFIGURATION_COMMAND_2				0x1FF95
#define PGN_GENERATOR_AC_STATUS_4				        0x1FF94
#define PGN_GENERATOR_ACFAULT_CONFIGURATION_STATUS_1	0x1FF93
#define PGN_GENERATOR_ACFAULT_CONFIGURATION_STATUS_2	0x1FF92
#define PGN_GENERATOR_ACFAULT_CONFIGURATION_COMMAND_1	0x1FF91
#define PGN_GENERATOR_ACFAULT_CONFIGURATION_COMMAND_2	0x1FF90
#define PGN_INVERTER_AC_STATUS_4				        0x1FF8F
#define PGN_INVERTER_ACFAULT_CONFIGURATION_STATUS_1		0x1FF8E
#define PGN_INVERTER_ACFAULT_CONFIGURATION_STATUS_2		0x1FF8D
#define PGN_INVERTER_ACFAULT_CONFIGURATION_COMMAND_1	0x1FF8C
#define PGN_INVERTER_ACFAULT_CONFIGURATION_COMMAND_2	0x1FF8B
#define PGN_CHARGER_AC_STATUS_4				            0x1FF8A
#define PGN_CHARGER_ACFAULT_CONFIGURATION_STATUS_1		0x1FF89
#define PGN_CHARGER_ACFAULT_CONFIGURATION_STATUS_2		0x1FF88
#define PGN_CHARGER_ACFAULT_CONFIGURATION_COMMAND_1		0x1FF87
#define PGN_CHARGER_ACFAULT_CONFIGURATION_COMMAND_2		0x1FF86
#define PGN_ATS_AC_STATUS_4				                0x1FF85
#define PGN_ATS_ACFAULT_CONFIGURATION_STATUS_1			0x1FF84
#define PGN_ATS_ACFAULT_CONFIGURATION_STATUS_2			0x1FF83
#define PGN_ATS_ACFAULT_CONFIGURATION_COMMAND_1			0x1FF82
#define PGN_ATS_ACFAULT_CONFIGURATION_COMMAND_2			0x1FF81
#define PGN_GENERATOR_DEMAND_STATUS				        0x1FF80
#define PGN_GENERATOR_DEMAND_COMMAND				    0x1FEFF
#define PGN_AGS_CRITERION_STATUS				        0x1FEFE
#define PGN_AGS_CRITERION_COMMAND				        0x1FEFD
#define PGN_FLOOR_HEAT_STATUS				            0x1FEFC
#define PGN_FLOOR_HEAT_COMMAND				            0x1FEFB
#define PGN_THERMOSTAT_STATUS_2				            0x1FEFA
#define PGN_THERMOSTAT_COMMAND_1				        0x1FEF9
#define PGN_THERMOSTAT_COMMAND_2				        0x1FEF8
#define PGN_THERMOSTAT_SCHEDULE_STATUS_1				0x1FEF7
#define PGN_THERMOSTAT_SCHEDULE_STATUS_2				0x1FEF6
#define PGN_THERMOSTAT_SCHEDULE_COMMAND_1				0x1FEF5
#define PGN_THERMOSTAT_SCHEDULE_COMMAND_2				0x1FEF4
#define PGN_AWNING_STATUS			                    0x1FEF3
#define PGN_AWNING_COMMAND				                0x1FEF2
#define PGN_TIRE_RAW_STATUS				                0x1FEF1
#define PGN_TIRE_STATUS				                    0x1FEF0
#define PGN_TIRE_SLOW_LEAK_ALARM				        0x1FEEF
#define PGN_TIRE_TEMPERATURE_CONFIGURATION_STATUS		0x1FEEE
#define PGN_TIRE_PRESSURE_CONFIGURATION_STATUS			0x1FEED
#define PGN_TIRE_PRESSURE_CONFIGURATION_COMMAND			0x1FEEC
#define PGN_TIRE_TEMPERATURE_CONFIGURATION_COMMAND		0x1FEEB
#define PGN_TIRE_ID_STATUS				                0x1FEEA
#define PGN_TIRE_ID_COMMAND				                0x1FEE9
#define PGN_INVERTER_DC_STATUS				            0x1FEE8
#define PGN_GENERATOR_DEMAND_CONFIGURATION_STATUS		0x1FEE7 // deprecated
#define PGN_GENERATOR_DEMAND_CONFIGURATION_COMMAND		0x1FEE6 // deprecated
#define PGN_LOCK_STATUS			                    	0x1FEE5
#define PGN_LOCK_COMMAND				                0x1FEE4
#define PGN_WINDOW_STATUS				                0x1FEE3
#define PGN_WINDOW_COMMAND				                0x1FEE2
#define PGN_DC_MOTOR_CONTROL_COMMAND				    0x1FEE1
#define PGN_DC_MOTOR_CONTROL_STATUS				        0x1FEE0
#define PGN_WINDOW_SHADE_CONTROL_COMMAND				0x1FEDF
#define PGN_WINDOW_SHADE_CONTROL_STATUS			    	0x1FEDE
#define PGN_AC_LOAD_STATUS_2            				0x1FEDD
#define PGN_DC_LOAD_STATUS_2			            	0x1FEDC
#define PGN_DC_DIMMER_COMMAND_3         				0x1FEDB
#define PGN_DC_DIMMER_STATUS_3			            	0x1FEDA
#define PGN_GENERIC_INDICATOR_COMMAND				    0x1FED9
#define PGN_GENERIC_CONFIGURATION_STATUS				0x1FED8
#define PGN_GENERIC_INDICATOR_STATUS			    	0x1FED7
#define PGN_MFG_SPECIFIC_CLAIM_REQUEST                  0x1FED6
#define PGN_AGS_DEMAND_CONFIGURATION_STATUS             0x1FED5
#define PGN_AGS_DEMAND_CONFIGURATION_COMMAND            0x1FED4
#define PGN_GPS_STATUS                                  0x1FED3
#define PGN_AGS_CRITERION_STATUS_2                      0x1FED2
#define PGN_SUSPENSION_AIR_PRESSURE_STATUS              0x1FED1
#define PGN_DC_DISCONNECT_STATUS                        0x1FED0
#define PGN_DC_DISCONNECT_COMMAND                       0x1FECF
#define PGN_INVERTER_CONFIGURATION_STATUS_3             0x1FECE
#define PGN_INTERTER_CONFIGURATION_COMMAND_3            0x1FECD
#define PGN_CHARGER_CONFIGURATION_STATUS_3              0x1FECC
#define PGN_CHARGER_CONFIGURATION_COMMAND_3             0x1FECB
#define PGN_DM_RV                                       0x1FECA
#define PGN_DC_SOURCE_STATUS_4                          0x1FEC9
#define PGN_DC_SOURCE_STATUS_5                          0x1FEC8
#define PGN_DC_SOURCE_STATUS_6                          0x1FEC7
#define PGN_GENERATOR_DC_STATUS_1                       0x1FEC6
#define PGN_GENERATOR_DC_CONFIGURATION_STATUS           0x1FEC5
#define PGN_GENERATOR_DC_COMMAND                        0x1FEC4
#define PGN_GENERATOR_DC_CONFIGURATION_COMMAND          0x1FEC3
#define PGN_GENERATOR_DC_EQUALIZATION_STATUS            0x1FEC2
#define PGN_GENERATOR_DC_EQUALIZATION_CONFIGURATION_STATUS  0x1FEC1
#define PGN_GENERATOR_DC_EQUALIZATION_CONFIGURATION_COMMAND  0x1FEC0
#define PGN_CHARGER_CONFIGURAITON_STATUS_4              0x1FEBF
#define PGN_CHARGER_CONFIGURATION_COMMAND_4             0x1FEBE
#define PGN_INVERTER_TEMPERATURE_STATUS                 0x1FEBD
#define PGN_HYDRAULIC_PUMP_COMMAND                      0x1FEBC
#define PGN_GENERIC_AC_STATUS_1                         0x1FEBB
#define PGN_GENERIC_AC_STATUS_2                         0x1FEBA
#define PGN_GENERIC_AC_STATUS_3                         0x1FEB9
#define PGN_GENERIC_AC_STATUS_4                         0x1FEB8
#define PGN_GENERIC_ACFAULT_CONFIGURATION_STATUS_1      0x1FEB7
#define PGN_GENERIC_ACFAULT_CONFIGURATION_STATUS_2      0x1FEB6
#define PGN_GENERIC_ACFAULT_CONFIGURATION_COMMAND_1     0x1FEB5
#define PGN_GENERIC_ACFAULT_CONFIGURATION_COMMAND_2     0x1FEB4
#define PGN_SOLAR_CONTROLLER_STATUS_1                   0x1FEB3
#define PGN_SOLAR_CONTROLLER_CONFIGURATION              0x1FEB2
#define PGN_SOLAR_CONTROLLER_COMMAND                    0x1FEB1
#define PGN_SOLAR_CONTROLLER_CONFIGURATION_COMMAND      0x1FEB0
#define PGN_SOLAR_EQUALIZATION_STATUS                   0x1FEAF
#define PGN_SOLAR_EQUALIZATION_CONFIGURATION_STATUS     0x1FEAE
#define PGN_SOLAR_EQUALIZATION_CONFIGURATION_COMMAND    0x1FEAD
#define PGN_DC_SOURCE_STATUS_7                          0x1FEAC
#define PGN_DC_SOURCE_STATUS_8                          0x1FEAB
#define PGN_DC_SOURCE_STATUS_9                          0x1FEAA
#define PGN_DC_SOURCE_STATUS_10                         0x1FEA9
#define PGN_CHASSIS_MOBILITY_STATUS_2                   0x1FEA8
#define PGN_ROOF_FAN_STATUS                             0x1FEA7
#define PGN_ROOF_FAN_COMMAND                            0x1FEA6
#define PGN_DC_SOURCE_STATUS_11                         0x1FEA5
#define PGN_DC_SOURCE_COMMAND                           0x1FEA4
#define PGN_CHARGER_STATUS_2                            0x1FEA3
#define PGN_CHARGER_CONFIGURATION_COMMAND_5             0x1FEA2
#define PGN_CHARGER_CONFIGURATION_STATUS_5              0x1FEA1
#define PGN_GPS_DATE_TIME_STATUS                        0x1FEA0
#define PGN_GENERIC_ALARM_STATUS                        0x1FE9F
#define PGN_GENERIC_ALARM_COMMAND                       0x1FE9E
#define PGN_INVERTER_CONFIGURATION_STATUS_4             0x1FE9B
#define PGN_INVERTER_CONFIGURATION_COMMAND_4            0x1FE9A
#define PGN_WATERHEATER_STATUS_2                        0x1FE99
#define PGN_WATERHEATER_COMMAND_2                       0x1FE98
#define PGN_CIRCULATION_PUMP_STATUS                     0x1FE97
#define PGN_CIRCULATION_PUMP_COMMAND                    0x1FE96
#define PGN_BATTERY_STATUS_1                            0x1FE95
#define PGN_BATTERY_STATUS_2                            0x1FE94
#define PGN_BATTERY_STATUS_3                            0x1FE93
#define PGN_BATTERY_STATUS_4                            0x1FE92
#define PGN_BATTERY_STATUS_5                            0x1FE91
#define PGN_BATTERY_STATUS_6                            0x1FE90
#define PGN_BATTERY_STATUS_7                            0x1FE8F
#define PGN_BATTERY_STATUS_8                            0x1FE8E
#define PGN_BATTERY_STATUS_9                            0x1FE8D
#define PGN_BATTERY_STATUS_10                           0x1FE8C
#define PGN_BATTERY_STATUS_11                           0x1FE8B
#define PGN_BATTERY_COMMAND                             0x1FE8A
#define PGN_STEP_STATUS                                 0x1FE89
#define PGN_STEP_COMMAND                                0x1FE88
#define PGN_VEHICLE_ENVIRONMENT_STATUS                  0x1FE87
#define PGN_VEHICLE_ENVIRONMENT_COMMAND                 0x1FE86




#define PGN_GENERAL_RESET	                			0x17F00
#define PGN_TERMINAL                    				0x17E00
#define PGN_DOWNLOAD		                    		0x17D00
#define PGN_INSTANCE_ASSIGNMENT			            	0x17C00
#define PGN_INSTANCE_STATUS			                	0x17B00

#define PGN_DC_COMPONENT_DRIVER_STATUS_1                0x16F00
#define PGN_DC_COMPONENT_DRIVER_STATUS_2                0x16E00
#define PGN_DC_COMPONENT_DRIVER_STATUS_3                0x16D00
#define PGN_DC_COMPONENT_DRIVER_STATUS_4                0x16C00
#define PGN_DC_COMPONENT_DRIVER_STATUS_5                0x16B00
#define PGN_DC_COMPONENT_DRIVER_SETTINGS_1              0x16A00
#define PGN_DC_COMPONENT_DRIVER_SETTINGS_2              0x16900
#define PGN_DC_COMPONENT_DRIVER_COMMAND                 0x16000
#define PGN_DC_COMPONENT_DRIVER_SETTINGS_COMMAND_1      0x16100
#define PGN_DC_COMPONENT_DRIVER_SETTINGS_COMMAND_2      0x16200



// Default start addresses
#define DSA_GENERATOR                                   64
#define DSA_GENERATOR_CONTROLLER                        65
#define DSA_INVERTER_1                                  66
#define DSA_INTERTER_2                                  67
#define DSA_CONTROL_PANEL                               68
#define DSA_BATTERY_CHARGE_MONITOR                      69
#define DSA_BATTERY                                     70
#define DSA_CHASSIS_BATTERY                             71
#define DSA_WATER_TANK                                  72
#define DSA_LPG_TANK                                    73
#define DSA_CONVERTER_1                                 74
#define DSA_CONVERTER_2                                 75
#define DSA_CHARGER_CONTROLLER                          76
#define DSA_AC_LOAD_CONTROLLER                          77
#define DSA_AC_FAULT_PROTECTION                         78
#define DSA_TRANSFER_SWITCH                             79
#define DSA_WEATHER_STATION                             80
#define DSA_LEVELING_SYSTEM_CONTROLLER                  81
#define DSA_LEVELING_SYSTEM                             82
#define DSA_AIR_LEVELING_SYSTEM                         83
#define DSA_SLIDE_ROOM_1                                84
#define DSA_SLIDE_ROOM_2                                85
#define DSA_SLIDE_ROOM_3                                86
#define DSA_SLIDE_ROOM_4                                87
#define DSA_MAIN_THERMOSTAT                             88
#define DSA_BEDROOM_THERMOSTAT                          89
#define DSA_THERMOSTAT_3                                90
#define DSA_THERMOSTAT_4                                91
#define DSA_THERMOSTAT_5                                92
#define DSA_THERMOSTAT_6                                93
#define DSA_MAIN_FURNACE                                94
#define DSA_CONVENTIONAL_FURNACE_2                      95
#define DSA_CONVENTIONAL_FURNACE_3                      96
#define DSA_AUX_HEAT_1                                  97
#define DSA_AUX_HEAT_2                                  98
#define DSA_AUX_HEAT_3                                  99
#define DSA_HYDRONIC_FURNACE                            100
#define DSA_WATER_HEATER_1                              101
#define DSA_WATER_HEATER_2                              102
#define DSA_AIR_CONDITIONER_1                           103
#define DSA_AIR_CONDITIONER_2                           104
#define DSA_AIR_CONDITIONER_3                           105
#define DSA_AIR_CONDITIONER_4                           106
#define DSA_REFRIGERATOR                                107
#define DSA_AUX_REFRIGERATOR                            108
#define DSA_AUX_FREEZER                                 109
#define DSA_ICE_MAKER                                   110
#define DSA_STOVE                                       111
#define DSA_AUDIO_ENTERTAINMENT_1                       112
#define DSA_AUDIO_ENTERTAINMENT_2                       113
#define DSA_AUDIO_ENTERTAINMENT_3                       114
#define DSA_VIDEO_ENTERTAINMENT_1                       115
#define DSA_VIDEO_ENTERTAINMENT_2                       116
#define DSA_VIDEO_ENTERTAINMENT_3                       117
#define DSA_TV_LIFT_1                                   118
#define DSA_TV_LIFT_2                                   119
#define DSA_GAS_DETECTOR_1                              120
#define DSA_GAS_DETECTOR_2                              121
#define DSA_GAS_DETECTOR_3                              122
#define DSA_GAS_DETECTOR_4                              123
#define DSA_GAS_DETECTOR_5                              124
#define DSA_GAS_DETECTOR_6                              125
#define DSA_ACTIVE_AIR_SUSPENSION                       126
#define DSA_WATER_PUMP                                  127
#define DSA_TANK_AUTOFILL                               128
#define DSA_WASTE_DUMP                                  129
#define DSA_AWNING                                      130
#define DSA_DC_DIMMER                                   131
#define DSA_DC_INPUT                                    132
#define DSA_TIRE_MONITOR                                133
#define DSA_WINDOW_SHADE_CONTROL                        134
#define DSA_DOOR_CONTROL                                135
#define DSA_GPS                                         136
#define DSA_AC_LOAD                                     137
#define DSA_DC_MOTOR_CONTROL                            138
#define DSA_DC_DISCONNECT                               139
#define DSA_GENERIC_AC_SOURCE                           140
#define DSA_SOLAR_CHARGE_CONTROLLER                     141
#define DSA_ROOF_FAN                                    142
#define DSA_EXTERNAL_INTERFACE                          143
#define DSA_GENERIC_ALARM                               144
#define DSA_WINDOW_CONTROL                              145
#define DSA_DC_LOAD                                     146
#define DSA_STEP_CONTROL                                147
#define DSA_SERVICE_TOOL                                249
#define DSA_SYSTEM_CLOCK                                250
#define DSA_DATA_LOGGER                                 251
#define DSA_CHASSIS_BRIDGE                              252
#define DSA_NETWORK_BRIDGE                              253


#define SPN_UNKNOWN                                     0
#define SPN_CPU                                         1
#define SPN_ADC                                         2
#define SPN_CONFIG                                      3
#define SPN_MEM                                         4
#define SPN_POWER                                       5
#define SPN_TEMPERATURE                                 6
#define SPN_CLOCK                                       7
#define SPN_DATE                                        8
#define SPN_RVC_CONNECTION                              9
#define SPN_SERIAL_NUMBER                               10
#define SPN_WATCHDOG                                    11
#define SPN_FIRMWARE                                    12
#define SPN_SUBNETWORK                                  13


#define FAIL_DATUM_ABOVE_RANGE                          0
#define FAIL_DATUM_BELOW_RANGE                          1
#define FAIL_DATUM_INVALID                              2
#define FAIL_SHORT_HIGH_VOLT                            3
#define FAIL_SHORT_LOW_VOLT                             4
#define FAIL_OPEN_CIRCUIT                               5
#define FAIL_GROUNDED                                   6
#define FAIL_DEVICE_NO_RESPONSE                         7
#define FAIL_DATUM_ERROR                                8
#define FAIL_DATUM_INVALID_RATE                         9
#define FAIL_DATUM_FLUCTUATING                          10
#define FAIL_NOT_IDENTIFIABLE                           11
#define FAIL_BAD_NODE                                   12
#define FAIL_CAL_REQUIRED                               13
#define FAIL_NOT_SPECIFIED                              14
#define FAIL_DATUM_VALID_ABOVE_RANGE                    15
#define FAIL_DATUM_VAILD_ABOVE_SEVERE                   16
#define FAIL_DATUM_VALID_BELOW_RANGE                    17
#define FAIL_DATUM_VALID_BELOW_SEVERE                   18
#define FAIL_INVALID_NETWORK_DATUM                      19
#define FAIL_UNKNOWN                                    31


#define ACK                                             0
#define NAK                                             1
#define CMD_NOT_ACCEPTED                                2
#define CMD_BAD_CONDITION                               3
#define CMD_NOT_FORMATTED                               4
#define CMD_PARAM_RANGE                                 5
#define CMD_REQUIRE_PWD                                 6
#define CMD_REQUIRE_TIME                                7
#define CMD_USER_OVERRIDE                               8






struct RVCpacket {
    uint8_t     priority,
    long_t      dgn,
    uint8_t     source,
    uint8_t     length,
    uint8_t     data[8],
}

#endif
